// console.log('mcom-script.js loaded');

// If true, start function. If false, listen for INIT.
window.onload = function() {
  if (Enabler.isInitialized()) {
    enablerInitHandler();
  } else {
    Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitHandler);
  }
}

function enablerInitHandler() {

// Dynamic Content variables and sample values (to be removed once live)
    Enabler.setProfileId(10025690);

    document.getElementById('description').innerHTML = dynamicContent.THMG_POC_feed_300x250_728x90[0].description;
    document.getElementById('button_text').innerHTML = dynamicContent.THMG_POC_feed_300x250_728x90[0].button_text;
    document.getElementById('slide1').style.backgroundImage = 'url(' + dynamicContent.THMG_POC_feed_300x250_728x90[0].background_image.Url + ')';

//Add button exit

//    document.getElementById('button').addEventListener('click', ctaHandler); 
    document.getElementById('ad-content').addEventListener('click', ctaHandler); 

function ctaHandler() { 
  Enabler.exitOverride(
      "Exit",
      dynamicContent.THMG_POC_feed_300x250_728x90[0].Exit_URL.Url); 
  }
    
}
