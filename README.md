# The Harley Medical Group dynamic creative

Version 1 of dynamic creative for DoubleClick Rich Media Studio. All main IAB sizes included with dynamic content managed within Studio.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- Login to [DoubleClick Studio](https://www.doubleclickbygoogle.com/solutions/digital-marketing/creative-solutions/).
- Text editor
- Sourcetree or similar Git client
- Ideally, localhost environment for testing.

### Installing

A step by step series of examples that tell you have to get a development env running

#### Set up local version

Clone git repo [via HTTPS](https://philwright@bitbucket.org/philwright/thmg-html5-creative.git)

#### Initialise GitFlow

In SourceTree, select **Repository > Git Flow / Hg Flow > Initialize repository** to set up Git Flow branch handling.

```

Git flow allows the use of Hotfix, Feature, and Release branches based off develop and master branches as appropriate.  
  
Keep all changes in the relevant branch – based on creative size – and merge back into develop on a daily basis.

```

## Deployment

Deployment is via publishing to DoubleClick Studio. 

- Ensure that all images required by the feed are uploaded and asset paths in the feed are updated accordingly.
- Ensure that all placeholder text has been removed before adding creative files back to Studio.

## Built With

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Phil Wright** - *Initial work*
* **Umair Yaquoob** - *Development of variant sizes*
* **Joe Eastham** - *Dynamic content set up and testing*